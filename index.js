const { ifError } = require('assert');
const fs = require('fs');



function promisify(fileName) {
    return new Promise((resolve, reject) => {
        fs.readFile(fileName, 'utf-8' ,function(err, data) {
            if (err) { 
                reject(err);
            } else {
                resolve(data);
            }
        })        
    })
}



// promisify("./data/1.txt")
//     .then((data) => { 
//         console.log(data);
//         return data.toUpperCase()
//     })
//     .then(console.log)
//     .catch(console.log);


async function fileRead(fileName) {
    const data = await promisify(fileName);
    Promise.all([data]).then(data => {
        console.log(data[0])
    })
}

fileRead("./data/2.txt");

function one(data) {
    console.log("from 1");
    return two(data)
}

function two(data) {
    console.log("from 2", data)
    return three(data);
}

function three(data) {
    console.log("from 3", data)
    return console.log("done!")
}

// one("This is the data");